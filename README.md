# Todo App UI

Generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.2.0, this project is a frontend of api TODO (https://gitlab.com/hmveng.app/my-todo-app/todo-app-api)

## Requirements
 Install https://gitlab.com/hmveng.app/my-todo-app/todo-app locally

## Development server (local)

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

