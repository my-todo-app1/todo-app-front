import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NgxsModule } from '@ngxs/store';
import { TranslateModule } from '@ngx-translate/core';
import { PublicTodoPageComponent } from './public-todo-page.component';

describe('PublicTodoPageComponent', () => {
  let component: PublicTodoPageComponent;
  let fixture: ComponentFixture<PublicTodoPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PublicTodoPageComponent, NgxsModule.forRoot(), TranslateModule.forRoot()]
    });
    fixture = TestBed.createComponent(PublicTodoPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
