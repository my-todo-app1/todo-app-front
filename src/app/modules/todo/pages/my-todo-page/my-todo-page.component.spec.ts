import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NgxsModule } from '@ngxs/store';
import { MessageService } from 'primeng/api';
import { TranslateModule } from '@ngx-translate/core';
import { MyTodoPageComponent } from './my-todo-page.component';

describe('MyTodoPageComponent', () => {
  let component: MyTodoPageComponent;
  let fixture: ComponentFixture<MyTodoPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MyTodoPageComponent, NgxsModule.forRoot(), TranslateModule.forRoot()],
      providers: [MessageService]
    });
    fixture = TestBed.createComponent(MyTodoPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
