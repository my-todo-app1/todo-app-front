import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NgxsModule } from '@ngxs/store';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';

import { RegisterPageComponent } from './register-page.component';
import { ToastService } from '@app/core/services/toast.service';
import { MessageService } from 'primeng/api';

describe('RegisterPageComponent', () => {
  let component: RegisterPageComponent;
  let fixture: ComponentFixture<RegisterPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RegisterPageComponent, NgxsModule.forRoot(), TranslateModule.forRoot(), RouterModule.forRoot([])],
      providers: [ToastService, MessageService]
    });
    fixture = TestBed.createComponent(RegisterPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
