import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';

import { FormControlPasswordComponent } from './form-control-password.component';

describe('FormControlPasswordComponent', () => {
  let component: FormControlPasswordComponent;
  let fixture: ComponentFixture<FormControlPasswordComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormControlPasswordComponent, TranslateModule.forRoot()]
    });
    fixture = TestBed.createComponent(FormControlPasswordComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
