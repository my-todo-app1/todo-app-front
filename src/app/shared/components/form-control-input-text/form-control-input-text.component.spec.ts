import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';

import { FormControlInputTextComponent } from './form-control-input-text.component';

describe('FormControlInputTextComponent', () => {
  let component: FormControlInputTextComponent;
  let fixture: ComponentFixture<FormControlInputTextComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormControlInputTextComponent, TranslateModule.forRoot()]
    });
    fixture = TestBed.createComponent(FormControlInputTextComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
