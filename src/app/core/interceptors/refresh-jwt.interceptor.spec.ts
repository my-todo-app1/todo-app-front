import { TestBed } from '@angular/core/testing';
import { NgxsModule } from '@ngxs/store';

import { RefreshJwtInterceptor } from './refresh-jwt.interceptor';

describe('RefreshJwtInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [NgxsModule.forRoot()],
    providers: [
      RefreshJwtInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: RefreshJwtInterceptor = TestBed.inject(RefreshJwtInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
