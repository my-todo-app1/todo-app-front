FROM node:21.6.1-alpine as build-stage

WORKDIR /app

COPY package*.json /app/
RUN npm install
COPY ./ /app/
ARG configuration=production
RUN npm run build -- --output-path=./dist/todo-app --configuration $configuration


FROM nginx:1.25.3-alpine

COPY --from=build-stage /app/dist/todo-app/ /user/share/nginx/html
COPY ./nginx-app.conf /etc/nginx/conf.d/default.conf