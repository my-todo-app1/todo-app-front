FROM nginx:1.25.3-alpine

#Copy app-dist
COPY ./dist/todo-app-angular /usr/share/nginx/html
#Copy default nginx configuration
COPY ./nginx-app.conf /etc/nginx/conf.d/default.conf

# add en variables
COPY ./set-env-variable.sh /docker-entrypoint.d/set-env-variable.sh
RUN chmod +x /docker-entrypoint.d/set-env-variable.sh